package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {
	@Query("select r from Report r where r.createdAt >= :start and r.createdAt <= :end order by id desc")
	List<Report> findByDate(@Param("start") Date startDate, @Param("end") Date endDate);
}

