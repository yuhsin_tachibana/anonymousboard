package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.service.CommentService;

@Controller
public class CommentController {

	@Autowired
	CommentService commentService;

	// 新規投稿画面
	@GetMapping("/comments/new/{id}")
	public ModelAndView newComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityの準備
		Comment comment = new Comment();
		comment.setReportId(id);
		// 画面遷移先を指定
		mav.setViewName("/comments/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", comment);
		return mav;
	}

	// 投稿処理
	@PostMapping("/comments/add")
	public ModelAndView addComment(@ModelAttribute("formModel") Comment comment) {
		// 投稿をテーブルに格納
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 編集画面
	@GetMapping("/comments/edit/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Comment comment = commentService.getComment(id);
		mav.setViewName("/comments/edit");
		mav.addObject("formModel", comment);
		return mav;
	}

	// 編集内容の更新
	@PutMapping("/comments/update/{id}")
	public ModelAndView updateComment(@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {
		comment.setId(id);
		commentService.saveComment(comment);
		return new ModelAndView("redirect:/");
	}

	// 削除機能
	@DeleteMapping("/comments/delete/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}

}
