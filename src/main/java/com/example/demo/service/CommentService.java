package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
@Transactional
public class CommentService {

	@Autowired
	CommentRepository commentRepository;

	// レコードの取得
	public Comment getComment(Integer id) {
		Comment comment = (Comment) commentRepository.findById(id).orElse(null);
		return comment;
	}

	// レコード追加・更新
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	// レコード削除
	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}

}
