package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
@Transactional
public class ReportService {

	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAllReport() {
		return reportRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
	}

	// 日付で絞り込み
	public List<Report> findByDateReport(String start, String end) throws ParseException {

		// 初期値の設定
		if (start.isBlank()) {
			start = "2021-01-01 00:00:00";
		} else {
			start += " 00:00:00";
		}

		if (end.isBlank()) {
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String formatDate = format.format(date);
			end = formatDate;
		} else {
			end += " 23:59:59";
		}

		// Date型に変換
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = sdFormat.parse(start);
        Date endDate = sdFormat.parse(end);
		return reportRepository.findByDate(startDate, endDate);
	}

	// レコード追加・更新
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// レコード削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	// レコードの取得
	public Report getReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

}
