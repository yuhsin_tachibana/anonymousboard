package com.example.demo.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "report")
public class Report {
	@Id
	@Column

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	private String content;

	@Column(name="created_at", insertable = false, updatable = false)
    private Date createdAt;

	@OneToMany(mappedBy = "report",cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
	@OrderBy("id DESC")
	private List<Comment> comments;

	// getter, setter
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	// commentsのgetter
	public List<Comment> getComments() {
		return comments;
	}

}
